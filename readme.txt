problems may be run using:
visualize_and_test_algorithm(file_path, x1_range, x2_range, algorithm, pop_size, num_generations, num_runs)
    """
    Visualizes the objective function and constraints for a 2D problem, runs the algorithm multiple times,
    and shows all Pareto fronts in a single graph.

    Parameters:
    - file_path: Path to the file containing the objective function and constraints.
    - x1_range: Tuple (min, max) defining the range for x1.
    - x2_range: Tuple (min, max) defining the range for x2.
    - algorithm: The algorithm function to run - either NSGA2 or stochastic ranking
    - pop_size: Population size for the algorithm.
    - num_generations: Number of generations for the algorithm.
    - num_runs: Number of times to run the algorithm. - default is 5
    """

or you may just run the main.ipynb file, but the resources folder has to be present in order to load problems